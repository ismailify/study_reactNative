import React, { useEffect, useState } from 'react';
import { View, ScrollView} from 'react-native';
import StylingReactComponent from './pages/StylingReactComponent/';
import SampleComponent from './pages/SampleComponent';
import FlexBox from './pages/FlexBox';
import Position from './pages/Layout';
import LifeCycle from './pages/LifeCycle';

const App = () => {
  const [isShow, setIsShow] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setIsShow(false)
    }, 5000);
  }, [])
  return (
    <View>
      <ScrollView>
        <SampleComponent />
        <StylingReactComponent />
        {isShow && <LifeCycle />}
        <Position />
        {isShow && <FlexBox />}
      </ScrollView>
    </View>
  )
}

export default App;